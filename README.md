# flume-databaseSink
quick to connect to a database as sink

## 文档、代码等有疑问的地方欢迎联系：mym0806@163.com，或提出issues，不胜感激

## 开发者文档已经上传：databaseSink.pdf
## 可运行版本已经发布，功能说明：
+ 支持insert操作全配置式
+ 支持快速扩展
+ 使用apache dpcp连接池
+ 暂时支持mysql


